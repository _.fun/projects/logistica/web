import { Home, Profile, Settings } from 'app/routing'

export const routing = { 
  Home,
  Profile,
  Settings,
}
