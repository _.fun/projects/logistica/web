import styled from 'styled'

export const Button = styled.button`
  font-size: 1em;
  border-radius: 3px;

  outline: none;
  cursor: pointer;
`
