import styled, { css, screen } from 'styled'

export const MenuPanel = styled.div`
  ${({ theme: { colors } }) => css`
    grid-area: panel;

    display: grid;
    grid-template: 'user' 'navigation';
    grid-auto-columns: 1fr;
    grid-auto-rows: min-content;

    color: ${colors.accent.main};
    background-color: ${colors.background.shade};

    a {
      text-decoration: none;
    }

    ${screen.min.medium(css`
      grid-template: 'navigation' 'user';
    `)}
  `};
`

export const NavigationBar = styled.div`
  ${() => css`
    grid-area: navigation;

    display: grid;
    grid-auto-flow: column;
    grid-auto-columns: 1fr;
    grid-auto-rows: min-content;

    text-align: center;
    overflow-x: auto;

    ${screen.min.medium(css`
      grid-auto-flow: row;
    `)}
  `};
`

export const UserBar = styled.div`
  ${() => css`
    grid-area: user;

    text-align: center;
    overflow-x: auto;


    ${screen.min.medium(css`
      align-self: end;
    `)}
  `};
`

export const NavItem = styled.div`
  ${({ theme: { spaces, colors } }) => css`
    padding ${spaces.x2} ${spaces.x4};
  
    color: ${colors.foreground.main};
    text-decoration: none;

    ${screen.max.small(css`
      margin-bottom: 2px;
    `)}

    ${screen.min.medium(css`
      margin-right: 2px;
    `)}

    &:hover {
      margin: 0;
      color: ${colors.accent.main};
      background-color: ${colors.background.main};

      ${screen.max.small(css`
        border-bottom: ${colors.accent.main} solid 2px;
      `)}

      ${screen.min.medium(css`
        border-right: ${colors.accent.main} solid 2px;
      `)}
    }

    .active & {
      margin: 0;
      color: ${colors.accent.main};
      background-color: ${colors.background.main};

      ${screen.max.small(css`
        border-bottom: ${colors.accent.main} solid 2px;
      `)}

      ${screen.min.medium(css`
        border-right: ${colors.accent.main} solid 2px;
      `)}
    }
  `}
`