import { Grid, Content } from './styled'
import { MenuPanel } from './menu'

export default {
  Grid,
  Content,
  MenuPanel,
}
