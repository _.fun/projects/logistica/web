import styled, { css, screen } from 'styled'

export const Grid = styled.div`
  ${({ theme: { spaces, colors } }) => css`
    min-height: 100vh;

    display: grid;
    grid-template:
      'panel'
      'content';
    grid-template-rows: auto 1fr;
    grid-gap: ${spaces.x3};

    background-color: ${colors.background.main};


    ${screen.min.medium(css`
      grid-template: 'panel content';
      grid-template-columns: auto 1fr;
    `)}
  `};
`

export const Content = styled.div`
  ${({ theme: { spaces, colors } }) => css`
    padding: ${spaces.x2} 0;
    width: 100%;

    grid-area: content;

    color: ${colors.foreground.main};
  `};
`
