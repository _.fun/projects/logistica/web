export interface IColors {
  accent: IColor
  foreground: IColor
  background: IColor
}

export interface IColor {
  main: string
  shade: string
}
