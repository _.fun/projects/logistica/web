import { dark } from './colors.dark'
import { light } from './colors.light'

export const colors = {
  dark,
  light,
}
