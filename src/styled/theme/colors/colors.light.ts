import { darken } from 'polished'
import { IColors } from './colors'

export const light: IColors = {
  accent: {
    main: 'blue',
    shade: 'blue',
  },
  foreground: {
    main: 'black',
    shade: 'black',
  },
  background: {
    main: '#fff',
    shade: darken(0.05, '#fff'),
  },
}
