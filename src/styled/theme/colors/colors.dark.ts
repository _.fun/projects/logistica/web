import { darken } from 'polished'
import { IColors } from './colors'

export const dark: IColors = {
  accent: {
    main: 'lightblue',
    shade: 'lightblue',
  },
  foreground: {
    main: 'white',
    shade: 'white',
  },
  background: {
    main: '#252526',
    shade: darken(0.03, '#252526'),
  },
}
