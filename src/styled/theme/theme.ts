import { IColors } from './colors/colors'
import { ISpaces } from './spaces/spaces'

export interface ITheme {
  name: string
  colors: IColors
  spaces: ISpaces
}
