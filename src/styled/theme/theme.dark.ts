import { ITheme } from './theme'
import { colors } from './colors'
import { spaces } from './spaces'

export const dark: ITheme = {
  name: 'dark',
  colors: colors.dark,
  spaces: spaces.dynamic,
}
