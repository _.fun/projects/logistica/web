import { ISpaces } from './spaces'

export const dynamic: ISpaces = {
  x0: '0em',
  x1: '0.5em',
  x2: '1em',
  x3: '1.5em',
  x4: '2em',
  x5: '2.5em',
  x6: '3em',
  x7: '3.5em',
  x8: '4em',
  x9: '4.5em',
  x10: '5em',
}
