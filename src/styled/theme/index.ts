
import { ITheme } from './theme'
import { light } from './theme.light'
import { dark } from './theme.dark'

export const themeList: ITheme[] = [light, dark]

export const themes = {
  light,
  dark,
}
