import { ITheme } from './theme'
import { colors } from './colors'
import { spaces } from './spaces'

export const light: ITheme = {
  name: 'default',
  colors: colors.light,
  spaces: spaces.dynamic,
}
