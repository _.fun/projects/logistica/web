import { css, FlattenSimpleInterpolation } from 'styled-components'

export interface ISize {
  big: number
  medium: number
  small: number
}

export interface IScreen {
  big: any
  medium: any
  small: any
}

export const sizes: ISize = {
  big: 992,
  medium: 768,
  small: 376,
}

export const mediaMax = (size: number) => (...style: any): FlattenSimpleInterpolation => css`
  @media (max-width: ${size / 16}em) {
    ${css(style)};
  }
`

export const mediaMin = (size: number) => (...style: any): FlattenSimpleInterpolation => css`
  @media (min-width: ${size / 16}em) {
    ${css(style)};
  }
`

export const max: IScreen = {
  big: mediaMax(sizes.big-1),
  medium: mediaMax(sizes.medium-1),
  small: mediaMax(sizes.medium-1),
}

export const min: IScreen = {
  big: mediaMin(sizes.big),
  medium: mediaMin(sizes.medium),
  small: mediaMin(sizes.medium),
}

export const screen = {
  max,
  min,
}
