import React, { FunctionComponent } from 'react'
import { Route, NavLink } from 'react-router-dom'

import { Profile as Component } from '.'

export const path = '/profile'

export const ProfileRoute: FunctionComponent = () => {
  console.log({ path })
  return (
  <Route path={path} component={Component} />
)}

export const ProfileNavigation: FunctionComponent = ({ children }) => (
  <NavLink to={path}>
    {children}
  </NavLink>
)

export const to: (history: any) => void = (history: any): void => {
  history.push(path)
}

export const Profile = {
  Route: ProfileRoute,
  Navigation: ProfileNavigation,
  to,
}
