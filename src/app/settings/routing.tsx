import React, { FunctionComponent } from 'react'
import { Route, NavLink } from 'react-router-dom'

import { Settings as Component } from '.'

export const path = '/settings'

export const SettingsRoute: FunctionComponent = () => (
  <Route path={path} component={Component} />
)

export const SettingsNavigation: FunctionComponent = ({ children }) => (
  <NavLink to={path}>
    {children}
  </NavLink>
)

export const to: (history: any) => void = (history: any): void => {
  history.push(path)
}

export const Settings = {
  Route: SettingsRoute,
  Navigation: SettingsNavigation,
  to,
}
