import React, { FunctionComponent } from 'react'
import { Route, Redirect, NavLink } from 'react-router-dom'

import { Home as Component } from '.'

export const path = '/home'

export const HomeRoute: FunctionComponent = () => (
  <Route path={path} component={Component} />
)

export const HomeNavigation: FunctionComponent = ({ children }) => (
  <NavLink to={path}>
    {children}
  </NavLink>
)

export const HomeRedirect: FunctionComponent = () => (
  <Redirect exact from='/' to={path} />
)

export const to: (history: any) => void = (history: any): void => {
  history.push(path)
}

export const Home = {
  Route: HomeRoute,
  Navigation: HomeNavigation,
  Redirect: HomeRedirect,
  to,
}
