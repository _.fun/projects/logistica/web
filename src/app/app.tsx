import React, { FunctionComponent } from 'react'
import {
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom'
import 'normalize.css'

import { themes, ThemeProvider } from 'styled'

import { Navigation } from 'components'
import { routing } from 'routing'

import { Menu } from './menu'

export const App: FunctionComponent = () => (
  <Router>
    <ThemeProvider theme={themes.dark}>

      <Navigation.Grid>

        <Navigation.MenuPanel>
          <Menu.User />
          <Menu.Navigation />
        </Navigation.MenuPanel>

        <Navigation.Content>

          <Switch>

            <routing.Home.Route />
            <routing.Profile.Route />
            <routing.Settings.Route />

          </Switch>

        </Navigation.Content>

      </Navigation.Grid>

    </ThemeProvider>
  </Router>
)
