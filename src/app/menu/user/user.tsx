import React, { FunctionComponent } from 'react'

import { Menu } from 'components/navigation'

export const User: FunctionComponent = () => (
  <Menu.UserBar>
    user
  </Menu.UserBar>
)
