import React, { FunctionComponent } from 'react'

import { routing } from 'routing'
import { Menu } from 'components/navigation'

export const Navigation: FunctionComponent = () => (
  <Menu.NavigationBar>

    <routing.Home.Navigation>
      <Menu.NavItem>home</Menu.NavItem>
    </routing.Home.Navigation>

    <routing.Profile.Navigation>
      <Menu.NavItem>profile</Menu.NavItem>
    </routing.Profile.Navigation>

    <routing.Settings.Navigation>
      <Menu.NavItem>settings</Menu.NavItem>
    </routing.Settings.Navigation>

  </Menu.NavigationBar>
)
