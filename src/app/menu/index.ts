import { Navigation } from './navigation'
import { User } from './user'

export const Menu = {
  Navigation,
  User,
}